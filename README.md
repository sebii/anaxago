# Lancement du projet

Editez le fichier .env.local pour y mettre ses propres variables d'environnement liées au test.

Lancez un ```composer update``` pour installer l'installation des composants nécessaires.

Ensuite faites un ```make start-project``` pour lancer les modifications systèmes liées au VirtualHost et fichier hosts de la machine.

# Utilisation

Le projet s'utilise tout simplement via le navigateur ici : http://starter.anaxago.local.com/

La documentation du endpoint est disponible via un navigateur ici : http://starter.anaxago.local.com/api

# Notes

Les Fixtures ont été aggémentées de plus de projets aléatoires, d'utilisateurs aléatoires, et de marques d'intérêts aléatoires.

J'ai également versé dans le dépôt un fichier de configuration NGINX car j'ai travaillé avec ce dernier au lieu d'Apache, il était déjà configuré sur ma machine de développement.
