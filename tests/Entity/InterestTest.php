<?php

namespace App\Tests\Entity;

use App\Entity\Interest;
use App\Entity\Project;
use App\Entity\User;
use Monolog\Test\TestCase;

class InterestTest extends TestCase
{
    public function testGoodInterestEntity(): void
    {
        $interest = (new Interest())
            ->setAmount(15)
            ->setProject(new Project())
            ->setUser(new User());
        $this->assertEquals(15, $interest->getAmount());
        $this->assertEquals(Project::class, get_class($interest->getProject()));
        $this->assertEquals(User::class, get_class($interest->getUser()));
    }

    public function testSetBadObjectInProject(): void
    {
        $this->expectException(\Error::class);
        $interest = (new Interest())
            ->setProject(new User());
    }

    public function testSetBadObjectInUser(): void
    {
        $this->expectException(\Error::class);
        $interest = (new Interest())
            ->setUser(new Project());
    }

    public function testBadAmount(): void
    {
        $this->expectException(\Exception::class);
        $interest = (new Interest())
            ->setAmount(-10);
    }
}