<?php

namespace App\Tests\Entity;

use App\Entity\Interest;
use App\Entity\Project;
use Monolog\Test\TestCase;

class ProjectTest extends TestCase
{
    public function testGoodProjectEntity(): void
    {
        $projectData = [
            'name' => 'Fred de la compta',
            'description' => 'Dépoussiérer la comptabilité grâce à l\'intelligence artificielle',
            'slug' => 'fred-compta',
            'created_at' => new \DateTime('now'),
            'requested_amount' => 2000,
        ];
        $project = (new Project())
            ->setTitle($projectData['name'])
            ->setSlug($projectData['slug'])
            ->setDescription($projectData['description'])
            ->setCreatedAt($projectData['created_at'])
            ->setRequestedAmount($projectData['requested_amount']);
        $this->assertEquals($projectData['name'], $project->getTitle());
        $this->assertEquals($projectData['slug'], $project->getSlug());
        $this->assertEquals($projectData['description'], $project->getDescription());
        $this->assertEquals($projectData['created_at'], $project->getCreatedAt());
        $this->assertEquals($projectData['requested_amount'], $project->getRequestedAmount());
        $this->assertEquals(false, $project->isFunded());
        $this->assertEquals(0, $project->getFundedAmount());
        $this->assertEquals(0, $project->getFundedPercent());
        $this->assertEquals(0, $project->getInterests()->count());
    }

    public function testRecalculateProjectFundedAmount(): void
    {
        $project = (new Project())
            ->addInterest((new Interest())->setAmount(200))
            ->addInterest((new Interest())->setAmount(300))
            ->recalculateFundedAmount();
        $this->assertEquals(2, $project->getInterests()->count());
        $this->assertEquals(Interest::class, get_class($project->getInterests()->first()));
        $this->assertEquals(500, $project->getFundedAmount());
        $this->assertEquals(true, $project->isFunded());
    }

    public function testFundedPercent(): void
    {
        $project = (new Project())
            ->setRequestedAmount(200)
            ->setFundedAmount(20)
            ->addFundedAmount(60);
        $this->assertEquals(80, $project->getFundedAmount());
        $this->assertEquals(40, $project->getFundedPercent());
    }
}