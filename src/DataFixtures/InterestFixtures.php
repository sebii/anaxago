<?php declare(strict_types = 1);

namespace App\DataFixtures;

use App\Entity\Interest;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class InterestFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $projects = $manager->getRepository(Project::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();
        $usersInProjects = [];
        foreach ($projects as $project) {
            $usersInProjects[$project->getUuid()->toString()] = [];
        }

        for ($i = 0; $i < 200; $i++) {
            $randUser = null;
            $randProject = null;
            while (is_null($randUser) && is_null($randProject)) {
                $randUser = $users[array_rand($users)];
                $randProject = $projects[array_rand($projects)];
                if (in_array($randUser->getUuid()->toString(), $usersInProjects[$randProject->getUuid()->toString()])) {
                    $randUser = null;
                    $randProject = null;
                } else {
                    $usersInProjects[$randProject->getUuid()->toString()][] = $randUser->getUuid()->toString();
                }
            }
            $interest = (new Interest())
                ->setUser($randUser)
                ->setProject($randProject)
                ->setAmount($faker->randomFloat(2, 10, 600));
            $manager->persist($interest);
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            ProjectFixtures::class,
            UserFixtures::class,
        ];
    }
}