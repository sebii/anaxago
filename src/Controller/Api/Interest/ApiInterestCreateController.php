<?php

namespace App\Controller\Api\Interest;

use App\Entity\Interest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiInterestCreateController
 * @package App\Controller\Api\Interest
 */
class ApiInterestCreateController extends AbstractController
{
    /**
     * @param Interest $data
     * @param EntityManagerInterface $em
     * @return Interest
     */
    public function __invoke(Interest $data, EntityManagerInterface $em): Interest
    {
        $data->setUser($this->getUser());
        $interest = $em->getRepository(Interest::class)
            ->findOneBy([
                'user' => $data->getUser(),
                'project' => $data->getProject(),
            ]);
        if (!is_null($interest)) {
            $interest->setAmount($data->getAmount());
            $data = $interest;
        }
        return $data;
    }
}