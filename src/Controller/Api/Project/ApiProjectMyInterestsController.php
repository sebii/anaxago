<?php

namespace App\Controller\Api\Project;

use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ApiProjectMyInterestsController extends AbstractController
{
    public function __invoke(Request $request, EntityManagerInterface $em)
    {
        return $em->getRepository(Project::class)->getAllByUserInterest($this->getUser());
    }
}