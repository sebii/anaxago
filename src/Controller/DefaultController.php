<?php declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('Default/index.html.twig');
    }

    /**
     * @return Response
     */
    public function myinterests(): Response
    {
        return $this->render('Default/myinterests.html.twig');
    }
}
