<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Api\Project\ApiProjectAllController;
use App\Controller\Api\Project\ApiProjectMyInterestsController;
use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"read:Project"}},
 *     collectionOperations={
 *         "all"={
 *             "method"="GET",
 *             "path"="/projects",
 *             "controller"=ApiProjectAllController::class
 *         },
 *         "myinterests"={
 *             "method"="GET",
 *             "path"="/projects/myinterests",
 *             "security"="is_granted('ROLE_USER')",
 *             "controller"=ApiProjectMyInterestsController::class
 *         }
 *     },
 *     itemOperations={
 *         "get"={
 *             "method"="GET",
 *             "path"="/project/{id}"
 *         }
 *     }
 * )
 */
class Project
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({"read:Project"})
     */
    private UuidInterface $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Groups({"read:Project"})
     */
    private string $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"read:Project"})
     */
    private string $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Groups({"read:Project"})
     */
    private string $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     * @Groups({"read:Project"})
     */
    private \DateTime $createdAt;

    /**
     * @var float
     * @ORM\Column(name="requested_amount", type="float", nullable=false, options={"default": 0})
     * @Groups({"read:Project"})
     */
    private float $requestedAmount;

    /**
     * @var float
     * @ORM\Column(name="funded_amount", type="float", nullable=false, options={"default": 0})
     * @Groups({"read:Project"})
     */
    private float $fundedAmount;

    /**
     * @var bool
     * @ORM\Column(name="funded", type="boolean", nullable=false, options={"default": false})
     * @Groups({"read:Project"})
     * @ApiProperty()
     */
    private bool $funded;

    /**
     * @var bool
     * @ORM\Column(name="email_sent", type="boolean", nullable=false, options={"default": false})
     * @Groups({"read:Project"})
     * @ApiProperty()
     */
    private bool $emailSent;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity=Interest::class, mappedBy="project")
     * @Groups({"read:Project"})
     */
    private Collection $interests;

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->emailSent = false;
        $this->funded = false;
        $this->fundedAmount = 0;
        $this->interests = new ArrayCollection();
        $this->requestedAmount = 0;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @param UuidInterface $uuid
     * @return Project
     */
    public function setUuid(UuidInterface $uuid): Project
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Project
     */
    public function setSlug(string $slug): Project
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Project
     */
    public function setTitle(string $title): Project
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Project
     */
    public function setDescription(string $description): Project
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt(\DateTime $createdAt): Project
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return float
     */
    public function getRequestedAmount(): float
    {
        return $this->requestedAmount;
    }

    /**
     * @param float $requestedAmount
     * @return Project
     * @throws \Exception
     */
    public function setRequestedAmount(float $requestedAmount): Project
    {
        if ($requestedAmount >= 0) {
            $this->requestedAmount = $requestedAmount;
        } else {
            throw new \Exception('Requested amount must be positive or null');
        }
        return $this;
    }

    /**
     * @return float
     */
    public function getFundedAmount(): float
    {
        return $this->fundedAmount;
    }

    /**
     * @param float $fundedAmount
     * @return Project
     * @throws \Exception
     */
    public function setFundedAmount(float $fundedAmount): Project
    {
        if ($fundedAmount >= 0) {
            $this->fundedAmount = $fundedAmount;
            $this->setFunded(($this->getFundedAmount() >= $this->getRequestedAmount()));
        } else {
            throw new \Exception('Funded amount must be positive or null');
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isFunded(): bool
    {
        return $this->funded;
    }

    /**
     * @param bool $funded
     * @return Project
     */
    public function setFunded(bool $funded): Project
    {
        $this->funded = $funded;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailSent(): bool
    {
        return $this->emailSent;
    }

    /**
     * @param bool $emailSent
     * @return Project
     */
    public function setEmailSent(bool $emailSent): Project
    {
        $this->emailSent = $emailSent;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getInterests(): Collection
    {
        return $this->interests;
    }

    /**
     * @param Collection $interests
     * @return Project
     */
    public function setInterests(Collection $interests): Project
    {
        $this->interests = $interests;
        return $this;
    }

    /**
     * @param Interest $interest
     * @return Project
     */
    public function addInterest(Interest $interest): Project
    {
        if (!$this->getInterests()->contains($interest)) {
            $this->getInterests()->add($interest);
        }
        return $this;
    }

    /**
     * @param Interest $interest
     * @return Project
     */
    public function removeInterest(Interest $interest): Project
    {
        if ($this->getInterests()->contains($interest)) {
            $this->getInterests()->removeElement($interest);
        }
        return $this;
    }

    /* ************************************************************* *
     * CUSTOM METHODS                                                *
     * ************************************************************* */

    /**
     * @param float $addFundedAmount
     * @return Project
     * @throws \Exception
     */
    public function addFundedAmount(float $addFundedAmount): Project
    {
        if ($addFundedAmount > 0) {
            $this->fundedAmount += $addFundedAmount;
            $this->setFunded(($this->getFundedAmount() >= $this->getRequestedAmount()));
        } else {
            throw new \Exception('Funded amount to add must be strictly positive.');
        }

        return $this;
    }

    /**
     * @return Project
     */
    public function recalculateFundedAmount(): Project
    {
        $this->fundedAmount = 0;
        foreach ($this->getInterests() as $interest) {
            $this->fundedAmount += $interest->getAmount();
        }
        $this->setFunded(($this->getFundedAmount() >= $this->getRequestedAmount()));
        return $this;
    }

    /**
     * @return int
     * @Groups({"read:Project"})
     */
    public function getFundedPercent(): int
    {
        return floor(($this->getFundedAmount() / $this->getRequestedAmount()) * 100);
    }
}

