<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Api\Interest\ApiInterestCreateController;
use App\Repository\InterestRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Interest
 * @package App\Entity
 * @ORM\Table(name="interest")
 * @ORM\Entity(repositoryClass=InterestRepository::class)
 * @ApiResource(
 *     denormalization_context={"groups"={"write:Interest"}},
 *     collectionOperations={
 *         "create"={
 *             "method"="POST",
 *             "path"="/interests",
 *             "security"="is_granted('ROLE_USER')",
 *             "controller"=ApiInterestCreateController::class
 *         }
 *     }
 * )
 */
class Interest
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({"read:Interest", "read:Project"})
     */
    private UuidInterface $uuid;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="interests")
     * @ORM\JoinColumn(name="user_uuid", referencedColumnName="uuid", nullable=false)
     * @Groups({"read:Interest", "read:Project"})
     */
    private User $user;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="interests")
     * @ORM\JoinColumn(name="project_uuid", referencedColumnName="uuid", nullable=false)
     * @Groups({"read:Interest", "write:Interest"})
     */
    private Project $project;

    /**
     * @var float
     * @ORM\Column(name="amount", type="float")
     * @Groups({"read:Interest", "write:Interest", "read:Project"})
     */
    private float $amount;

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @param UuidInterface $uuid
     * @return Interest
     */
    public function setUuid(UuidInterface $uuid): Interest
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Interest
     */
    public function setUser(User $user): Interest
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Interest
     */
    public function setProject(Project $project): Interest
    {
        $this->project = $project;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Interest
     * @throws \Exception
     */
    public function setAmount(float $amount): Interest
    {
        if ($amount > 0) {
            $this->amount = $amount;
        } else {
            throw new \Exception('Amount must be strictly positive');
        }
        return $this;
    }
}