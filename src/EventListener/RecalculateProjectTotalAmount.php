<?php

namespace App\EventListener;

use App\Entity\Interest;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class RecalculateProjectTotalAmount implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postRemove,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Interest) {
            return ;
        }

        $em = $args->getObjectManager();
        $project = $entity->getProject();
        $project->addFundedAmount($entity->getAmount());
        $em->flush();
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Interest) {
            return ;
        }

        $em = $args->getObjectManager();
        $project = $entity->getProject();
        $project->recalculateFundedAmount();
        $em->flush();
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Interest) {
            return ;
        }

        $em = $args->getObjectManager();
        $project = $entity->getProject();
        $project->recalculateFundedAmount();
        $em->flush();
    }
}