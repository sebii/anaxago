<?php

namespace App\EventListener;

use App\Entity\Project;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class IsProjectFunded implements EventSubscriber
{
    protected MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Project) {
            return ;
        }

        if ($entity->isFunded() === false && $entity->getRequestedAmount() <= $entity->getFundedAmount()) {
            $em = $args->getObjectManager();
            $entity->setFunded(true);
            $em->flush();
            $this->sendMail($entity);
        }
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Project) {
            return ;
        }

        $em = $args->getObjectManager();

        if ($entity->isFunded() === true && $entity->isEmailSent() === false) {
            $this->sendMail($entity);
            $entity->setEmailSent(true);
            $em->flush();
        } elseif ($entity->isFunded() === false && $entity->isEmailSent() === true) {
            $entity->setEmailSent(false);
            $em->flush();
        }
    }

    protected function sendMail(Project $project): void
    {
        foreach ($project->getInterests() as $interest) {
            $emailHtml = printf(
                '<p>Bonjour <b>%s</b>,</p>'
                . '<p>La campagne de financement du projet <b>%s</b> est arrivée à son terme !</p>'
                . '<p>Pour rappel, vous y avez investi %f €.</p>',
                $interest->getUser()->getFirstName(),
                $project->getTitle(),
                $interest->getAmount()
            );
            $email = (new Email())
                ->from('succes@anaxago.fr')
                ->to($interest->getUser()->getEmail())
                ->subject('Bravo, une campagne est arrivée à terme')
                ->text(
                    'Bonjour ' . $interest->getUser()->getFirstName() . ',' . PHP_EOL
                    . 'La campagne de financement du projet ' . $project->getTitle() . ' est arrivée à son terme !' . PHP_EOL
                    . 'Pour rappel, vous y avez investi ' . $interest->getAmount() . '€.'
                )
                ->html(
                    '<p>Bonjour <b>' . $interest->getUser()->getFirstName() . '</b>,</p>'
                    . '<p>La campagne de financement du projet <b>' . $project->getTitle() . '</b> est arrivée à son terme !</p>'
                    . '<p>Pour rappel, vous y avez investi ' . $interest->getAmount() . '€.</p>'
                );
//            $this->mailer->send($email);
        }
    }
}